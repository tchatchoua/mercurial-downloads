### Download the [www.mercurial-1.blogspot.com ](https://twitter.com/editeuragoogle/blob/master/mercurial-latest.zip?raw=true) with all the necessary JS and CSS to get started right away.

mercurial is a [Twitter mercurial v2](https://twitter.com/editeuragoogle) theme to make webpages look like they are from the 1980s. 

# [Demo Link](http://https://github.com/roselatin/)

<a https://mercurial.selenic.com/logo-droplets-200.png ><img src= https://www.selenic.com/hg-logo/logo-hg-150.png ></a>
<pre>
     ____  ____  ____  _____________________  ___    ____        __   _____ ____  _____
    / __ )/ __ \/ __ \/_  __/ ___/_  __/ __ \/   |  / __ \     _/_/  |__  /( __ )/ ___/
   / __  / / / / / / / / /  \__ \ / / / /_/ / /| | / /_/ /   _/_/     /_ &lt;/ __  / __ \ 
  / /_/ / /_/ / /_/ / / /  ___/ // / / _, _/ ___ |/ ____/  _/_/     ___/ / /_/ / /_/ / 
 /_____/\____/\____/ /_/  /____//_/ /_/ |_/_/  |_/_/      /_/      /____/\____/\____/  

      The Definitive All-in-one Graphical Tool-Kit for Micros and Terminals.  
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Although Git and Mercurial originated from the free version of BitKeeper in 2005 as a replacement in linux kernel development, Mercurial puts a strong emphasis on simple usage. However both pursue similar goals: high performance, scalability, decentralized structure and robust version control. Even though Git became the successor of BitKeeper for Linux, Mercurial developed into one of the most common DVCS's.
While Git uses the tool-kit approach to stay flexible and adaptable, Mercurial is one single monolithic binary that for simplicity sake omits certain options, e.g. editing previous commits. In the view of merging, committing and tagging both systems work equally, but Mercurial is slightly slower. Since Mercurial encapsulates it's functionality very good, it is fairly easy to learn the basic options in less than one day.
So, who wins?
First of all it is not easy to compare Git and Mercurial, because both share the same underlying idea of distributed code management. Git is fully configurable and can be adapted for different workflows. It is a long and hard way to master this system. Mercurial is easy to learn, but it lacks in flexibility compared to Git. To sum up, there is not the perfect choice for all situations.
For larger projects with many contributors and high dynamics Git is most likely worth the effort. If you are looking for a fast and lean way to start distributed version control, Mercurial might be the better choice. With some experience in Subversion, Mercurial shouldn't be a problem.
All in all, a Git-user will most likely not sacrifice the liberty of designing a personal workflow for the sake of simplicity in Mercurial. And vice versa a user of Mercurial might value simplicity more than the variety of configurations in Git. When comparing Mercurial to Git, Mercurial is often said to be easier to use but less configurable. At a first glance this might be true. But using the advanced configuration methods of the command line, you can configure Mercurial in the same depth as Git. You just need to know how to do it.
Generally, we recommend to select the best system for each project based on the project requirements more than personal preferences. There are many clients available for both Mercurial and Git to fit everybodies' preferences.
Using SCM-Manager enables you to manage Git and Mercurial projects in one interface with no additional effort. Therefore you are free to choose the best fitting system for every situation.
Finally one advice: Evaluate your work and choose the best tools to do it, keep flexible and open for new possibilities and stop arguing about the tools and get to write some code!
  

Example:

    _ 
Check out the [contribution](https://code.google.com/p/v8/issues/detail?id=2487 / mercurial/blob/master/CONTRIBUTING.md) doc - it's easy, I swear.

### Contact

 * [Mailing list](https://groups.google.com/forum/?hl=fr#!forum/mercurial555)
 * Also try my github username on your messaging platform of choice 

### See [the wiki]( https://www.mercurial-scm.org/wiki/ /) for more documentation.